package com.cainkilgore.wandy;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class WandyCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender s, Command c, String l, String [] args) {
		if(!(s instanceof Player)) {
			if(args.length < 1) {
				s.sendMessage("Please specify a player to give a wandy to..");
				return true;
			}
			if(args.length > 0) {
				Player p = Bukkit.getPlayer(args[0]);
				if(p != null) {
					p.getInventory().addItem(Wandy.wandy);
					s.sendMessage("You've given " + p.getName() + " a wandy.. careful.");
					p.sendMessage(ChatColor.RED + "Console has given you a Wandy, be careful.");
					// return true;
				}
			}
			return true;
		}
		
		Player player = (Player) s;
		
		if(!player.isOp()) {
			player.sendMessage(ChatColor.RED + "Sorry, you must be an operator to get a Wandy.");
			return true;
		}
		
		player.getInventory().addItem(Wandy.wandy);
		player.sendMessage(ChatColor.AQUA + "This is wandy. Take care of it.");
		return true;
	}

}
