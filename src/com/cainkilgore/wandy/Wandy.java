package com.cainkilgore.wandy;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Wandy extends JavaPlugin {
	
	public static ItemStack wandy = new ItemStack(Material.STICK, 1);
	public static Wandy plugin;
	
	public void onEnable() {
		plugin = this;
		ItemMeta wandymeta = wandy.getItemMeta();
		wandymeta.setDisplayName(ChatColor.DARK_BLUE + "[" + ChatColor.RED + ChatColor.BOLD + "Wandy" + ChatColor.DARK_BLUE + "]" + ChatColor.RESET);
		wandymeta.addEnchant(Enchantment.LUCK, 1, true);
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Wandy the Wand");
		wandymeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		wandymeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		wandymeta.setLore(lore);
		wandy.setItemMeta(wandymeta);
		
		System.out.println("Wandy has been enabled v1.01");
		this.getCommand("wandy").setExecutor(new WandyCommand());
		Bukkit.getPluginManager().registerEvents(new WandySelector(), this);
		Bukkit.getPluginManager().registerEvents(new WandyFireball(), this);
		Bukkit.getPluginManager().registerEvents(new WandyTeleport(), this);
		Bukkit.getPluginManager().registerEvents(new WandyFly(), this);
		Bukkit.getPluginManager().registerEvents(new WandyCreeper(), this);
		Bukkit.getPluginManager().registerEvents(new WandyThrow(), this);
		Bukkit.getPluginManager().registerEvents(new WandyBlock(), this);
		Bukkit.getPluginManager().registerEvents(new WandyRocket(), this);
		
		new BukkitRunnable() {
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()) {
					if(WandySelector.playerModes.containsKey(p.getUniqueId())) {
						BossBar bar = Bukkit.createBossBar("Wand Mode: " + ChatColor.RED + WandySelector.playerModes.get(p.getUniqueId()).toString().toUpperCase(), BarColor.RED, BarStyle.SOLID, BarFlag.DARKEN_SKY);
						if(p.getItemInHand().equals(wandy)) {
								bar.addPlayer(p);
								new BukkitRunnable() {
									public void run() {
										bar.removeAll();
									}
								}.runTaskLater(Wandy.plugin, 1);
								// bar.removeAll();
						} else {
							bar.removePlayer(p);
						}
					}
				}
			}
		}.runTaskTimer(this, 1, 1);
	}
	
	public void onDisable() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			removeWandy(p);
		}
	}
	
	public void removeWandy(Player player) {
		System.out.println("Scanning " + player.getName() + "'s inventory contents..");
		System.out.println(player.getInventory().getContents().toString());
		for(ItemStack items : player.getInventory().getContents()) {
			if(items != null) {
				if(items.equals(wandy)) {
					player.getInventory().remove(wandy);
				}
			}
		}
	}
	
	

}
