package com.cainkilgore.wandy;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class WandySelector implements Listener {
	
	public static HashMap<UUID, WandyTypes> playerModes = new HashMap<UUID, WandyTypes>();
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getItem() == null) return;
		Player player = e.getPlayer();
		
		if(WandySelector.wandInHand(e.getPlayer())) {
			
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				
				if(!playerModes.containsKey(player.getUniqueId())) {
					setWandType(player, WandyTypes.FIREBALL);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.FIREBALL) {
					setWandType(player, WandyTypes.CREEPER);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.CREEPER) {
					setWandType(player,WandyTypes.TELEPORT);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.TELEPORT) {
					setWandType(player, WandyTypes.FLY);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.FLY) {
					setWandType(player, WandyTypes.THROW);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.THROW) {
					setWandType(player, WandyTypes.BLOCK);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.BLOCK) {
					setWandType(player, WandyTypes.ROCKET);
					return;
				}
				
				if(playerModes.get(player.getUniqueId()) == WandyTypes.ROCKET) {
					setWandType(player, WandyTypes.FIREBALL);
					return;
				}
				
				
			}
			return;
		}
	}
	
	public void setWandType(Player player, WandyTypes type) {
		if(playerModes.get(player.getUniqueId()) == null) {
			playerModes.put(player.getUniqueId(), type);
		} else {
			playerModes.replace(player.getUniqueId(), type);
		}
		
		player.sendMessage(ChatColor.DARK_RED + "Switched wand mode to " + ChatColor.RED + ">> " + ChatColor.AQUA + type.toString().toUpperCase() + ChatColor.RED + " <<");
		// player.sendMessage("Debug: " + player.getName() + " is now set to " + playerModes.get(player.getUniqueId()));
	}
	
	
	public static boolean hasSelected(Player player, WandyTypes type) {
		if(playerModes.get(player.getUniqueId()) != null) {
			if(playerModes.get(player.getUniqueId()) == type) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	public static boolean wandInHand(Player player) {
		if(player.getItemInHand() != null) {
			if(player.getItemInHand().equals(Wandy.wandy)) {
				return true;
			}
			return false;
		}
		return false;
	}

}
