package com.cainkilgore.wandy;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class WandyCreeper implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(WandySelector.hasSelected(e.getPlayer(), WandyTypes.CREEPER)) {
			if(WandySelector.wandInHand(e.getPlayer())) {
				if(e.getAction() != Action.LEFT_CLICK_AIR) return;
				Player player = e.getPlayer();
				Block block = player.getTargetBlock((Set<Material>) null, 100);
				
				int yRel = 0;
				
				for(int i = 1; i < 250; i++) {
					if(block.getLocation().add(0, i, 0).getBlock().getType() == Material.AIR) {
						yRel = i;
						break;
					}
				}
				
				block = block.getRelative(0, yRel, 0);
				
				Creeper creep = player.getWorld().spawn(block.getLocation(), Creeper.class);
				player.getWorld().strikeLightningEffect(creep.getLocation());
				for(int x = -1; x < 3; x++) {
					for(int z = -1; z < 3; z++) {
						creep.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, creep.getLocation().add(x, 0, z), 5);
					}
				}
				
				for(Entity en : creep.getNearbyEntities(100, 100, 100)) {
					if(en instanceof LivingEntity) {
						creep.setTarget((LivingEntity)en);
					}
				}
				
				new BukkitRunnable() {
					public void run() {
						creep.setGlowing(false);
					}
				}.runTaskLater(Wandy.plugin, 20 * 10);
				
				creep.setGlowing(true);
			}
		}
	}
	
}
