package com.cainkilgore.wandy;

import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class WandyRocket implements Listener {

	@EventHandler
	public void onPlayerDamageEntity(EntityDamageByEntityEvent e) {
		if(e.getDamager() instanceof Player) {
			Player player = (Player) e.getDamager();
			if(WandySelector.hasSelected(player, WandyTypes.ROCKET)) {
				if(WandySelector.wandInHand(player)) {
					Entity damaged = e.getEntity();
					e.setCancelled(true);
					damaged.setGravity(false);
					damaged.setVelocity(new Vector(0, 1, 0));
					new BukkitRunnable() {
						public void run() {
							damaged.getWorld().spawnParticle(Particle.HEART, damaged.getLocation(), 1);
							if(damaged.getVelocity().getY() <= 0) {
								damaged.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, damaged.getLocation(), 5);
								damaged.remove();
								this.cancel();
							}
						}
					}.runTaskTimer(Wandy.plugin, 1, 1);
				}
			}
		}
	}
	
}
