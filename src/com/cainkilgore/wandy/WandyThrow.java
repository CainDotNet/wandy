package com.cainkilgore.wandy;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class WandyThrow implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(WandySelector.hasSelected(e.getPlayer(), WandyTypes.THROW)) {
			if(WandySelector.wandInHand(e.getPlayer())) {
				if(e.getAction() != Action.LEFT_CLICK_AIR) return;
				Player player = e.getPlayer();
				Block block = player.getTargetBlock((Set<Material>) null, 100);

				for(Entity en : block.getWorld().getNearbyEntities(block.getLocation(), 10, 10, 10)) {
					
					if(en instanceof Player) {
						Player p = (Player) en;
						if(p == player) continue;
					}
					
					en.setVelocity(new Vector(Math.random()*2,Math.random()*2,Math.random()*2));
					en.getWorld().spawnParticle(Particle.SMOKE_LARGE, en.getLocation(), 5);
					
					new BukkitRunnable() {
						int runtime = 0;
						public void run() {
							runtime++;
							if(en.getVelocity().getY() < 0) {
								this.cancel();
							}
							if(runtime > 100) {
								this.cancel();
							}
							en.getWorld().spawnParticle(Particle.FALLING_DUST, en.getLocation(), 5);
						}
					}.runTaskTimer(Wandy.plugin, 1, 1);
					
					new BukkitRunnable() {
						int runtime = 0;
						public void run() {
							runtime++;
							if(runtime > 5) {
								if(en.getLocation().add(0,-1,0).getBlock().getType() != Material.AIR) {
									en.getWorld().playSound(en.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1L, 1L);
									en.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, en.getLocation(), 1);
									if(en instanceof Animals) {
										en.remove();
									}
									this.cancel();
								}
							}
							
							if(runtime > 100) {
								this.cancel();
							}
						}
					}.runTaskTimer(Wandy.plugin, 1, 1);
				}
			}
		}
	}

}
