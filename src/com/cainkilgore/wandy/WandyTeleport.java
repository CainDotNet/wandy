package com.cainkilgore.wandy;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class WandyTeleport implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(WandySelector.hasSelected(e.getPlayer(), WandyTypes.TELEPORT)) {
			if(WandySelector.wandInHand(e.getPlayer())) {
				if(e.getAction() != Action.LEFT_CLICK_AIR) return;
				Player player = e.getPlayer();
				Block block = player.getTargetBlock((Set<Material>) null, 100);
				final Location lastLoc = player.getLocation();
				final Vector direction = player.getLocation().getDirection();
				int yRel = 0;
				
				for(int i = 1; i < 250; i++) {
					if(block.getLocation().add(0, i, 0).getBlock().getType() == Material.AIR) {
						yRel = i;
						break;
					}
				}
				
				if(block.getType() == Material.AIR) {
					player.playSound(player.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.2F, 0.2F);
					return;
				}
				player.teleport(block.getLocation().add(0,yRel,0).setDirection(direction));
				player.getLocation().setDirection(direction);
				lastLoc.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, lastLoc, 2);
				player.getWorld().spawnParticle(Particle.CLOUD, player.getLocation(),  5);
			}
		}
	}
	

}
