package com.cainkilgore.wandy;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LargeFireball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class WandyFireball implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(WandySelector.hasSelected(e.getPlayer(), WandyTypes.FIREBALL)) {
			if(WandySelector.wandInHand(e.getPlayer())) {
				if(e.getAction() != Action.LEFT_CLICK_AIR) return;
				Fireball fireball = e.getPlayer().launchProjectile(LargeFireball.class);
				fireball.setBounce(true);
				fireball.setVelocity(e.getPlayer().getLocation().getDirection());
				new BukkitRunnable() {
					int i = 0;
					public void run() {
						i++;
						if(!fireball.isDead()) {
							fireball.getWorld().playEffect(fireball.getLocation(), Effect.SMOKE, 15);
							// fireball.getWorld().createExplosion(fireball.getLocation(), 1, false);
							fireball.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, fireball.getLocation(), 20);
							fireball.getWorld().playSound(fireball.getLocation(), Sound.ENTITY_FIREWORK_SHOOT, 0.5F, 0.5F);
						} else {
							this.cancel();
						}
						
						if(i > 200) {
							fireball.remove();
							this.cancel();
						}
						
						if(fireball.getLocation().getBlock().getType() == Material.STATIONARY_WATER) {
							this.cancel();
							fireball.remove();
						}
					}
				}.runTaskTimer(Wandy.plugin, 1, 1);
			}
		}
	}

}
