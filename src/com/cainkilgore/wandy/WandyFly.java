package com.cainkilgore.wandy;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scheduler.BukkitRunnable;

public class WandyFly implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(WandySelector.hasSelected(e.getPlayer(), WandyTypes.FLY)) {
			if(WandySelector.wandInHand(e.getPlayer())) {
				if(e.getAction() != Action.LEFT_CLICK_AIR) return;
				if(e.getPlayer().getLocation().add(0,-1,0).getBlock().getType() == Material.AIR) return;
				
				Player player = e.getPlayer();
				
				new BukkitRunnable() {
					int runtime = 0;
					public void run() {
						runtime++;
						player.setAllowFlight(true);

						player.setVelocity(player.getLocation().getDirection());
						if(runtime > 200) {
							this.cancel();
						}
						if(runtime > 20) {
							if(player.getLocation().add(0,-1,0).getBlock().getType() != Material.AIR) {
								this.cancel();
							}
						}
						if(player.isSneaking()) {
							this.cancel();
						}
						
						player.getWorld().spawnParticle(Particle.FALLING_DUST, player.getLocation(), 100);
					}
				}.runTaskTimer(Wandy.plugin, 1, 1);
				
				new BukkitRunnable() {
					public void run() {
						if(player.getVelocity().getY() <= 0) {
							if(player.getLocation().add(0,-1,0).getBlock().getType() != Material.AIR) {
								player.setAllowFlight(false);
								player.sendMessage("Allow flight set to false");
								this.cancel();
							}
						}
					}
				}.runTaskTimer(Wandy.plugin, 1, 1);
			}
		}
	}

}
