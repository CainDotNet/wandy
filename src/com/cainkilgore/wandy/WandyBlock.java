package com.cainkilgore.wandy;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class WandyBlock implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(WandySelector.hasSelected(e.getPlayer(), WandyTypes.BLOCK)) {
			if(WandySelector.wandInHand(e.getPlayer())) {
				if(e.getAction() != Action.LEFT_CLICK_AIR) return;
				Player player = e.getPlayer();
				Block block = player.getTargetBlock((Set<Material>) null, 100);
				
				for(int x = -1; x < 2; x++) {
					for(int z = -1; z < 2; z++) {
						Block multiblock = block.getLocation().add(x,0,z).getBlock();
						Material multimat = multiblock.getType();
						multiblock.setType(Material.AIR);
						
						FallingBlock falling = multiblock.getWorld().spawnFallingBlock(multiblock.getLocation().add(0,1,0), multimat, multiblock.getData());
						falling.setInvulnerable(true);
						falling.setCustomName("Bob");
						falling.setCustomNameVisible(true);
						
						new BukkitRunnable() {
							int runtime = 0;
							public void run() {
								falling.setVelocity(new Vector(0, 0.5, 0));
								runtime++;
								falling.getWorld().spawnParticle(Particle.FALLING_DUST, falling.getLocation(), 1);
								if(runtime > 200) {
									falling.remove();
									this.cancel();
								}
							}
						}.runTaskTimer(Wandy.plugin, 1, 1);
					}
				}
			}
		}
	}

}
